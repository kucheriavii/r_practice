install.packages("pacman")
require("pacman")

#встановити пакети за допомогою pacman
pacman::p_load(pacman, dplyr, GGally, ggplot2, ggthemes, 
               ggvis, httr, lubridate, plotly, rio, rmarkdown, shiny, 
               stringr, tidyr)
p_unload(all)

detach("package:datasets", unload = TRUE)  # For base
cat('\014')
